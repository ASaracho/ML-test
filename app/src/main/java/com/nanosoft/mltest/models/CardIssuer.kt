package com.nanosoft.mltest.models

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.annotations.SerializedName
import com.nanosoft.mltest.R
import com.skocken.efficientadapter.lib.viewholder.EfficientViewHolder
import com.squareup.picasso.Picasso
import io.realm.RealmObject
import java.io.Serializable

/**
 * Created by asaracho on 1/11/18.
 */

open class CardIssuer : RealmObject(), Serializable {

    var id: String? = null
    var name: String? = null
    var thumbnail: String? = null
    @SerializedName("secure_thumbnail")
    var secureThumbnail: String? = null
    @SerializedName("processing_mode")
    var processingMode: String? = null
    @SerializedName("merchant_account_id")
    var merchantAccountId: String? = null

    class ViewHolder(itemView: View) : EfficientViewHolder<CardIssuer>(itemView) {
        override fun updateView(context: Context, cardIssuer: CardIssuer) {
            var tittle = findViewByIdEfficient<TextView>(R.id.tittle)
            var description = findViewByIdEfficient<TextView>(R.id.description)
            var pic = findViewByIdEfficient<ImageView>(R.id.pic)

            Picasso.with(pic.context).load(cardIssuer.thumbnail).into(pic);
            tittle.setText(cardIssuer.name);
            description.visibility = View.GONE;
        }
    }
}