package com.nanosoft.mltest;

/**
 * Created by asaracho on 1/11/18.
 */

import com.nanosoft.mltest.models.CardIssuer;
import com.nanosoft.mltest.models.Installment;
import com.nanosoft.mltest.models.PaymentMethod;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import java.util.List;

@Rest(rootUrl = "https://api.mercadopago.com/v1/payment_methods", converters = {GsonHttpMessageConverter.class})
public interface MLRestApi extends RestClientErrorHandling {
    @Get("?public_key={key}")
    List<PaymentMethod> getPaymentMethods(@Path String key);

    @Get("/card_issuers?public_key={key}&payment_method_id={paymentMethodId}")
    List<CardIssuer> getCardIssuers(@Path String key, @Path String paymentMethodId);

    @Get("/installments?public_key={key}&amount={amount}&payment_method_id={PaymentMethodId}&cardIssuer.id={IssuerId}")
    List<Installment> getInstallments(@Path String key, @Path String PaymentMethodId,
                                      @Path float amount, @Path String IssuerId);
}
