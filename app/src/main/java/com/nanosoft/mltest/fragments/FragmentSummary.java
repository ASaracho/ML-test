package com.nanosoft.mltest.fragments;

import com.nanosoft.mltest.R;
import com.nanosoft.mltest.databinding.FSummaryBinding;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.BindingObject;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.DataBound;
import org.androidannotations.annotations.EFragment;

/**
 * Created by asaracho on 1/12/18.
 */

@DataBound
@EFragment(R.layout.f_summary)
public class FragmentSummary extends FragmentTicket {

    @BindingObject
    FSummaryBinding binding;

    @AfterViews
    void afterViews() {
        binding.setPurchase(getPurchase());
    }

    @Click(R.id.confirm)
    void confirmPurchase() {
        goToNextStep();
    }
}
