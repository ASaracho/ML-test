package com.nanosoft.mltest.fragments;

import android.view.View;
import android.widget.Button;

import com.nanosoft.mltest.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.text.ParseException;

import faranjit.currency.edittext.CurrencyEditText;

/**
 * Created by asaracho on 1/12/18.
 */

@EFragment(R.layout.f_amount_input)
public class FragmentAmountInput extends FragmentTicket {

    @ViewById
    Button bNext;

    @ViewById
    CurrencyEditText etAmount;

    @TextChange(R.id.et_amount)
    void etAmountChange() {
        try {
            if (etAmount.getCurrencyDouble() > 0) {
                bNext.setVisibility(View.VISIBLE);
                return;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        bNext.setVisibility(View.GONE);
    }

    @Click(R.id.b_next)
    void goToNext() {
        try {
            getPurchase().setAmount((float) etAmount.getCurrencyDouble());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        goToNextStep();
    }

}
