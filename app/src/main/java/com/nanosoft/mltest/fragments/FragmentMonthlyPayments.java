package com.nanosoft.mltest.fragments;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nanosoft.mltest.R;
import com.nanosoft.mltest.Utils;
import com.nanosoft.mltest.models.Installment;
import com.nanosoft.mltest.models.PayerCost;
import com.nanosoft.mltest.models.Purchase;
import com.skocken.efficientadapter.lib.adapter.EfficientAdapter;
import com.skocken.efficientadapter.lib.adapter.EfficientRecyclerAdapter;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by asaracho on 1/12/18.
 */

@EFragment(R.layout.f_monthly_payments)
public class FragmentMonthlyPayments extends FragmentTicket implements EfficientAdapter.OnItemClickListener {

    @ViewById
    TextView tittle, description;

    @ViewById
    ImageView pic;

    @ViewById
    RecyclerView rv;

    private Installment mInstallment;

    EfficientRecyclerAdapter<PayerCost> adapter =
            new EfficientRecyclerAdapter<>(R.layout.simple_list_item,
                    PayerCost.ViewHolder.class,
                    new ArrayList<PayerCost>());

    @AfterViews
    void afterViews() {
        tittle.setText(getPurchase().getCardIssuer().getName());
        description.setText(getPurchase().getPaymentMethod().getName());
        Picasso.with(getContext()).load(getPurchase().getCardIssuer().getThumbnail()).into(pic);


        Utils.prepareRecyclerView(rv, adapter, this);
        getPayerCosts();
    }

    @Background
    void getPayerCosts() {
        showLoadingAnimation();
        Purchase p = getPurchase();
        List<Installment> installments = api.getInstallments(
                getString(R.string.ML_API_KEY),
                p.getPaymentMethod().getId(),
                p.getAmount(),
                p.getCardIssuer().getId());
        if (installments.size() > 0) mInstallment = installments.get(0);
        hideLoadingAnimation();
        refreshList();
    }

    @UiThread
    void refreshList() {
        if (mInstallment == null || mInstallment.getPayerCosts() == null || mInstallment.getPayerCosts().size() == 0) {
            Snackbar.make(getView(), R.string.no_items, Snackbar.LENGTH_LONG).show();
            goBack();
        }
        adapter.updateWith(mInstallment.getPayerCosts());
    }

    @Override
    public void onItemClick(EfficientAdapter adapter, View view, Object object, int position) {
        getPurchase().setPayerCost((PayerCost) adapter.get(position));
        goToNextStep();
    }
}
