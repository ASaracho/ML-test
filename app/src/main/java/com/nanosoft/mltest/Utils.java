package com.nanosoft.mltest;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.skocken.efficientadapter.lib.adapter.EfficientAdapter;
import com.skocken.efficientadapter.lib.adapter.EfficientRecyclerAdapter;


/**
 * Created by Alan on 14/01/2018.
 */

public class Utils {
    public static void prepareRecyclerView(RecyclerView rv, EfficientRecyclerAdapter adapter, EfficientAdapter.OnItemClickListener listener){
        Context context = rv.getContext();
        adapter.setOnItemClickListener(listener);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(new ColorDrawable(context.getResources().getColor(R.color.colorAccent)));
        rv.addItemDecoration(dividerItemDecoration);
        rv.setLayoutManager(new LinearLayoutManager(context));
        rv.setAdapter(adapter);
    }
}
