package com.nanosoft.mltest;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.nanosoft.mltest.fragments.FragmentAmountInput_;
import com.nanosoft.mltest.fragments.FragmentBankSelection_;
import com.nanosoft.mltest.fragments.FragmentMonthlyPayments_;
import com.nanosoft.mltest.fragments.FragmentPaymentMethod_;
import com.nanosoft.mltest.fragments.FragmentSummary_;
import com.nanosoft.mltest.models.FragmentScreen;
import com.nanosoft.mltest.models.Purchase;
import com.nanosoft.mltest.views.SelectableStepView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.UUID;

@EActivity(R.layout.a_main)
public class ActivityNewPurchase extends AppCompatActivity implements SelectableStepView.onStepSelectedListener, StepNavigator {

    @ViewById
    SelectableStepView sv;

    @ViewById
    FrameLayout container;

    private ArrayList<FragmentScreen> screens = new ArrayList<>();
    private Purchase purchase = new Purchase();
    private int currentStep;

    @AfterViews
    void afterViews() {
        initializeFragments();
        initializeStepView();
        goToScreen(currentStep);
    }

    void initializeFragments() {
        screens.add(new FragmentScreen("Precio", FragmentAmountInput_.class));
        screens.add(new FragmentScreen("Metodo\nde pago", FragmentPaymentMethod_.class));
        screens.add(new FragmentScreen("Banco", FragmentBankSelection_.class));
        screens.add(new FragmentScreen("Cuotas", FragmentMonthlyPayments_.class));
        screens.add(new FragmentScreen("Confirmar", FragmentSummary_.class));
    }

    void initializeStepView() {
        ArrayList<String> steps = new ArrayList<>();
        for (FragmentScreen screen : screens) steps.add(screen.getScreenName());
        sv.setSteps(steps);
        sv.setOnStepSelectedListener(this);
    }

    void goToScreen(int position) {

        Fragment fragment = null;

        try {
            fragment = (Fragment) screens.get(position).getFragment().getConstructor().newInstance();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (fragment == null) return;

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.right_in,
                        R.animator.right_out,
                        R.animator.left_in,
                        R.animator.left_out)
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
        currentStep = position;
        sv.go(currentStep, true);
    }

    @Override
    public void onStepSelected(int step) {
        if (step < currentStep) {
            for (int i = currentStep; i > step; i--)
                getSupportFragmentManager().popBackStack();
            currentStep = step;
            sv.go(currentStep, true);
        }
    }

    @Override
    public void goToNextStep() {
        if (currentStep == sv.getStepCount() - 1)
            saveAndClose();
        else {
            currentStep++;
            goToScreen(currentStep);
        }
    }

    @Override
    public void goBack() {
        currentStep--;
        sv.go(currentStep, false);
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public Purchase getPurchase() {
        return purchase;
    }

    @Override
    public void saveAndClose() {
        Intent intent = new Intent();
        intent.putExtra("purchase", purchase);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (currentStep > 0)
            goBack();
        else
            finish();
    }
}
