package com.nanosoft.mltest.fragments;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.nanosoft.mltest.R;
import com.nanosoft.mltest.Utils;
import com.nanosoft.mltest.models.CardIssuer;
import com.skocken.efficientadapter.lib.adapter.EfficientAdapter;
import com.skocken.efficientadapter.lib.adapter.EfficientRecyclerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by asaracho on 1/12/18.
 */

@EFragment(R.layout.f_bank_selection)
public class FragmentBankSelection extends FragmentTicket implements EfficientAdapter.OnItemClickListener {

    @ViewById
    RecyclerView rv;

    EfficientRecyclerAdapter<CardIssuer> adapter =
            new EfficientRecyclerAdapter<>(R.layout.simple_list_item,
                    CardIssuer.ViewHolder.class,
                    new ArrayList<CardIssuer>());

    @AfterViews
    void afterViews() {
        Utils.prepareRecyclerView(rv, adapter, this);
        downloadCardIssuers();
    }

    @Background
    void downloadCardIssuers() {
        showLoadingAnimation();
        List<CardIssuer> cardIssuers =
                api.getCardIssuers(getString(R.string.ML_API_KEY), getPurchase().getPaymentMethod().getId());
        hideLoadingAnimation();
        refreshCardIssuersList(cardIssuers);
    }

    @UiThread
    void refreshCardIssuersList(List<CardIssuer> cardIssuers) {
        adapter.updateWith(cardIssuers);
        if(cardIssuers.size() == 0){
            Snackbar.make(getView(), R.string.no_items, Snackbar.LENGTH_LONG).show();
            goBack();
        }
    }

    @Override
    public void onItemClick(EfficientAdapter adapter, View view, Object object, int position) {
        getPurchase().setCardIssuer((CardIssuer) adapter.get(position));
        goToNextStep();
    }
}
