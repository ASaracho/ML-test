package com.nanosoft.mltest;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Alan on 14/01/2018.
 */

public class MLAplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
