package com.nanosoft.mltest;

import com.nanosoft.mltest.models.Purchase;

/**
 * Created by Alan on 14/01/2018.
 */

public interface StepNavigator {

    void goToNextStep();

    void goBack();

    void saveAndClose();

    Purchase getPurchase();
}
