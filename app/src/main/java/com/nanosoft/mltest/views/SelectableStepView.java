package com.nanosoft.mltest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

import com.shuhart.stepview.StepView;

/**
 * Created by asaracho on 1/12/18.
 */

public class SelectableStepView extends StepView {

    private onStepSelectedListener onStepSelectedListener;

    public SelectableStepView(Context context) {
        super(context);
    }

    public SelectableStepView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectableStepView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(onStepSelectedListener == null) return false;

        if(event.getAction() == MotionEvent.ACTION_DOWN){
            int step = (int) event.getX() / (getWidth() / getStepCount());
            onStepSelectedListener.onStepSelected(step);
        }

        return true;
    }

    public void setOnStepSelectedListener(onStepSelectedListener stepSelectedListener) {
        this.onStepSelectedListener = stepSelectedListener;
    }

    public interface onStepSelectedListener{
        void onStepSelected(int step);
    }

}
