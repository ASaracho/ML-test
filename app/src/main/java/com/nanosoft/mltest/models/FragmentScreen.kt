package com.nanosoft.mltest.models

/**
 * Created by asaracho on 1/12/18.
 */
class FragmentScreen(var screenName: String, var fragment: Class<*>)