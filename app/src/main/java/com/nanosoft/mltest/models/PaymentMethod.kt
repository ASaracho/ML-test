package com.nanosoft.mltest.models

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.annotations.SerializedName
import com.nanosoft.mltest.R
import com.skocken.efficientadapter.lib.viewholder.EfficientViewHolder
import com.squareup.picasso.Picasso
import io.realm.RealmObject
import java.io.Serializable

/**
 * Created by asaracho on 1/11/18.
 */
open class PaymentMethod : Serializable, RealmObject() {
    var id: String? = null
    var name: String? = null
    @SerializedName("payment_type_id")
    var paymentTypeId: String? = null
    var status: String? = null
    @SerializedName("secure_thumbnail")
    var secureThumbnail: String? = null
    var thumbnail: String? = null
    @SerializedName("deferred_capture")
    var deferredCapture: String? = null

    class ViewHolder(itemView: View) : EfficientViewHolder<PaymentMethod>(itemView) {
        override fun updateView(context: Context, paymentMethod: PaymentMethod) {
            var tittle = findViewByIdEfficient<TextView>(R.id.tittle)
            var description = findViewByIdEfficient<TextView>(R.id.description)
            var pic = findViewByIdEfficient<ImageView>(R.id.pic)

            Picasso.with(pic.context).load(paymentMethod.thumbnail).into(pic);
            tittle.setText(paymentMethod.name);
            description.visibility = View.GONE;
        }
    }

}