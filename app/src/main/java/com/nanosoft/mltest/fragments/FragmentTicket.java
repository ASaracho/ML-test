package com.nanosoft.mltest.fragments;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

import com.nanosoft.mltest.MLRestApi;
import com.nanosoft.mltest.StepNavigator;
import com.nanosoft.mltest.models.Purchase;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.rest.spring.annotations.RestService;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;

/**
 * Created by asaracho on 1/12/18.
 */

@EFragment
abstract public class FragmentTicket extends Fragment implements RestErrorHandler {

    @FragmentArg
    Purchase purchase;

    @RestService
    MLRestApi api;

    StepNavigator nav;

    FragmentDialogLoading_ loadingDialog = new FragmentDialogLoading_();

    @AfterInject
    void afterInject() {
        api.setRestErrorHandler(this);
        try {
            nav = (StepNavigator) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement StepNavigator");
        }
    }

    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {
        Snackbar.make(getView(), "Oops! Algo malio sal", Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(getResources().getColor(android.R.color.white))
                .setAction("REPORTAR", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(view, "JAJAJAAJAJAJAJAAJJAJA", Snackbar.LENGTH_LONG).show();
                    }
                }).show();
    }

    @UiThread
    void showLoadingAnimation() {
        loadingDialog.show(getChildFragmentManager(), null);
    }

    @UiThread
    void hideLoadingAnimation() {
        loadingDialog.dismiss();
    }

    public Purchase getPurchase() {
        return nav.getPurchase();
    }

    void goToNextStep() {
        nav.goToNextStep();
    }

    void goBack() {
        nav.goBack();
    }
}
