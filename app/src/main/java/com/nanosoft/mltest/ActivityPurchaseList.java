package com.nanosoft.mltest;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nanosoft.mltest.models.Purchase;
import com.skocken.efficientadapter.lib.adapter.EfficientAdapter;
import com.skocken.efficientadapter.lib.adapter.EfficientRecyclerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Alan on 14/01/2018.
 */

@EActivity(R.layout.activity_purchase_list)
public class ActivityPurchaseList extends AppCompatActivity implements EfficientAdapter.OnItemClickListener, EfficientAdapter.OnItemLongClickListener<Purchase> {

    public static final int CODE_NEW_PURCHASE = 23843;

    @ViewById
    RecyclerView rv;
    @ViewById
    View emptyList;

    Realm realm = Realm.getDefaultInstance();

    EfficientRecyclerAdapter<Purchase> adapter =
            new EfficientRecyclerAdapter<>(R.layout.simple_list_item,
                    Purchase.ViewHolder.class, new ArrayList<Purchase>());

    @AfterViews
    void afterViews() {
        Utils.prepareRecyclerView(rv, adapter, this);
        adapter.setOnItemLongClickListener(this);
    }

    void refreshData() {
        RealmResults<Purchase> results = realm.where(Purchase.class).findAll();
        adapter.updateWith(results);
        refreshNoDataMessage();
    }

    void refreshNoDataMessage(){
        if (adapter.size() == 0)
            emptyList.setVisibility(View.VISIBLE);
        else
            emptyList.setVisibility(View.GONE);

    }

    @Click(R.id.fab)
    void openNewPurchase() {
        Intent intent = new Intent(this, ActivityNewPurchase_.class);
        startActivityForResult(intent, CODE_NEW_PURCHASE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_NEW_PURCHASE && resultCode == RESULT_OK) {
            Purchase purchase = (Purchase) data.getSerializableExtra("purchase");
            realm.beginTransaction();
            realm.copyToRealm(purchase);
            realm.commitTransaction();
            refreshData();
        }
    }

    @Override
    public void onLongItemClick(final EfficientAdapter<Purchase> adapter, View view, final Purchase object, final int position) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Eliminar");
        dialog.setMessage("Desea eliminar este registro?");

        dialog.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                realm.beginTransaction();
                adapter.removeAt(position);
                object.deleteFromRealm();
                realm.commitTransaction();
                refreshNoDataMessage();
            }
        });

        dialog.show();
    }

    @Override
    public void onItemClick(EfficientAdapter adapter, View view, Object object, int position) {
        //show detail data???
    }
}
