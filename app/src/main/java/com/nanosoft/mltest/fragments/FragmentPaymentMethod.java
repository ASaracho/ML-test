package com.nanosoft.mltest.fragments;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.nanosoft.mltest.R;
import com.nanosoft.mltest.Utils;
import com.nanosoft.mltest.models.PaymentMethod;
import com.skocken.efficientadapter.lib.adapter.EfficientAdapter;
import com.skocken.efficientadapter.lib.adapter.EfficientRecyclerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by asaracho on 1/12/18.
 */

@EFragment(R.layout.f_payment_method)
public class FragmentPaymentMethod extends FragmentTicket implements EfficientAdapter.OnItemClickListener {

    @ViewById
    RecyclerView rv;

    EfficientRecyclerAdapter<PaymentMethod> adapter =
            new EfficientRecyclerAdapter<>(R.layout.simple_list_item,
                    PaymentMethod.ViewHolder.class,
                    new ArrayList<PaymentMethod>());


    @AfterViews
    void afterViews() {
        Utils.prepareRecyclerView(rv, adapter, this);
        loadPaymentMethods();
    }

    @Background
    void loadPaymentMethods() {
        showLoadingAnimation();
        List<PaymentMethod> paymentMethods = api.getPaymentMethods(getString(R.string.ML_API_KEY));
        hideLoadingAnimation();
        refreshRv(paymentMethods);
    }

    @UiThread
    void refreshRv(List<PaymentMethod> paymentMethods) {
        adapter.updateWith(paymentMethods);
        if(paymentMethods.size() == 0){
            Snackbar.make(getView(), R.string.no_items, Snackbar.LENGTH_LONG).show();
            goBack();
        }
    }

    @Override
    public void onItemClick(EfficientAdapter adapter, View view, Object object, int position) {
        getPurchase().setPaymentMethod((PaymentMethod) adapter.get(position));
        goToNextStep();
    }
}
