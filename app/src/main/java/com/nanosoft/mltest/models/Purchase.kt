package com.nanosoft.mltest.models

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.nanosoft.mltest.R
import io.realm.RealmObject
import java.io.Serializable
import com.skocken.efficientadapter.lib.viewholder.EfficientViewHolder
import com.squareup.picasso.Picasso
import io.realm.annotations.PrimaryKey
import java.util.*


/**
 * Created by asaracho on 1/11/18.
 */

open class Purchase() : Serializable, RealmObject() {

    @PrimaryKey
    var id: String? = UUID.randomUUID().toString()
    var amount: Float = 0f
    var payerCost: PayerCost? = null
    var paymentMethod: PaymentMethod? = null
    var cardIssuer: CardIssuer? = null

    class ViewHolder(itemView: View) : EfficientViewHolder<Purchase>(itemView) {
        override fun updateView(context: Context, purchase: Purchase) {
            var tittle = findViewByIdEfficient<TextView>(R.id.tittle)
            var description = findViewByIdEfficient<TextView>(R.id.description)
            var pic = findViewByIdEfficient<ImageView>(R.id.pic)

            tittle.text = "${purchase.cardIssuer!!.name} (${purchase.paymentMethod!!.name})"
            description.text = purchase.payerCost!!.recommendedMessage
            Picasso.with(pic.context).load(purchase.cardIssuer!!.thumbnail).into(pic)
        }
    }

}