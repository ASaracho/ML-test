package com.nanosoft.mltest.models

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.annotations.SerializedName
import com.nanosoft.mltest.R
import com.skocken.efficientadapter.lib.viewholder.EfficientViewHolder
import io.realm.RealmObject
import java.io.Serializable

/**
 * Created by asaracho on 1/11/18.
 */
open class PayerCost : Serializable, RealmObject() {

    var installments: Int = 0
    @SerializedName("installment_amount")
    var installmentAmount: Double = 0.0
    @SerializedName("total_amount")
    var totalAmount: Double = 0.0
    @SerializedName("recommended_message")
    var recommendedMessage: String? = null

    class ViewHolder(itemView: View) : EfficientViewHolder<PayerCost>(itemView) {
        override fun updateView(context: Context, payerCost: PayerCost) {
            var tittle = findViewByIdEfficient<TextView>(R.id.tittle)
            var description = findViewByIdEfficient<TextView>(R.id.description)
            var pic = findViewByIdEfficient<ImageView>(R.id.pic)

            pic.visibility = View.GONE;
            description.visibility = View.GONE;
            tittle.setText(payerCost.recommendedMessage);
        }
    }
}