package com.nanosoft.mltest.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import java.io.Serializable

/**
 * Created by asaracho on 1/11/18.
 */

open class Installment : Serializable, RealmObject() {

    @SerializedName("payment_method_id")
    var paymentMethodId: String? = null
    @SerializedName("payment_type_id")
    var paymentTypeId: String? = null
    @SerializedName("card_issuer")
    var cardIssuer: CardIssuer? = null
    @SerializedName("processing_mode")
    var processingMode: String? = null
    @SerializedName("merchant_account_id")
    var merchantAccountId: Int = 0
    @SerializedName("payer_costs")
    var payerCosts: RealmList<PayerCost>? = RealmList()
}